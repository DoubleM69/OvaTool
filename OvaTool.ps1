﻿[Void][System.Reflection.Assembly]::LoadWithPartialName('presentationframework') 
foreach ($item in $(gci .\assembly\ -Filter *.dll).name) {
    [Void][System.Reflection.Assembly]::LoadFrom("assembly\$item")
}
#########################################################################
#                     Variable + Import Module                          #
#########################################################################
$path = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Definition)
$pathTool="$path"+"\Tool\"
$ext=".ova"

[void] [Reflection.Assembly]::LoadWithPartialName( 'System.Windows.Forms' )

#Import-Module -Name "$Path\services\functions.psm1"

#########################################################################
#                        Load Main Panel                                #
#########################################################################

$Global:pathPanel= split-path -parent $MyInvocation.MyCommand.Definition

function LoadXaml ($filename){
    $XamlLoader=(New-Object System.Xml.XmlDocument)
    $XamlLoader.Load($filename)
    return $XamlLoader
}
$XamlMainWindow=LoadXaml($pathPanel+"\main.xaml")
$reader = (New-Object System.Xml.XmlNodeReader $XamlMainWindow)
$Form = [Windows.Markup.XamlReader]::Load($reader)



#########################################################################
#                       Functions Base                   								#
#########################################################################
#region window
$XamlMainWindow.SelectNodes("//*[@Name]") | %{
    try {Set-Variable -Name "$("WPF_"+$_.Name)" -Value $Form.FindName($_.Name) -ErrorAction Stop}
    catch{throw}
    }
 
Function Get-FormVariables{
  if ($global:ReadmeDisplay -ne $true){Write-host "If you need to reference this display again, run Get-FormVariables" -ForegroundColor Yellow;$global:ReadmeDisplay=$true}
  write-host "Found the following interactable elements from our form" -ForegroundColor Cyan
  get-variable *WPF*
}

$WPF_Theme.Add_Click({
  $Theme1 = [ControlzEx.Theming.ThemeManager]::Current.DetectTheme($form)
   $my_theme = ($Theme1.BaseColorScheme)
 If($my_theme -eq "Light")
   {
           [ControlzEx.Theming.ThemeManager]::Current.ChangeThemeBaseColor($form,"Dark")
           $WPF_Theme.ToolTip = "Theme Dark"
   }
 ElseIf($my_theme -eq "Dark")
   {					
           [ControlzEx.Theming.ThemeManager]::Current.ChangeThemeBaseColor($form,"Light")
           $WPF_Theme.ToolTip = "Theme Light"
   }		
})

$WPF_BaseColor.Add_Click({

  $Script:Colors=@()
  $Accent = [ControlzEx.Theming.ThemeManager]::Current.ColorSchemes
  foreach ($item in $Accent)
  {
      $Script:Colors += $item
  }

  $Value = $Script:Colors[$(Get-Random -Minimum 0 -Maximum 23)]
  [ControlzEx.Theming.ThemeManager]::Current.ChangeThemeColorScheme($form ,$Value)
  $WPF_BaseColor.ToolTip = "BaseColor : $Value"

})

$WPF_Gitlab.Add_Click({
 #start https://gitlab.com/Vikzer
 #start https://gitlab.com/EBMBA
 Start-Process https://gitlab.com/DoubleM69
})
#endregion
  #Get-FormVariables
#########################################################################
#                       Functions                       								#
#########################################################################
function Validate-IsEmptyTrim ([string] $field) {

  if($field -eq $null -or $field.Trim().Length -eq 0) {
    return $true    
  }
      
  return $false
}

# function change ovf to ova on extension's files
function ChangeExtensionOVFtoOVA ([string] $File) {
  $result = $File.Replace(".ovf",".ova")
  return $result
}

#########################################################################
#                       DATA       						       		                #
#########################################################################
$WPF_Param.Add_Click({
  if ($WPF_Param.IsChecked -eq $true) {
    $WPF_ParamPanel.Visibility = "Visible"
  }
  else {
    $WPF_ParamPanel.Visibility = "Hidden"
  }
})

$WPF_OvaDir.Add_Click({
  if ($WPF_OvaDir.IsChecked -eq $true ) {
    $WPF_OvaPanel.Visibility = "Visible"
  }
  else {
    $WPF_OvaPanel.Visibility = "Collapsed"
  }
})


#region browse file
$openfiledialog1 = New-Object System.Windows.Forms.OpenFileDialog
$openfiledialog1.DefaultExt = "ovf"
$openfiledialog1.Filter = "Applications (*.ovf) |*.ovf"
$openfiledialog1.ShowHelp = $True
$openfiledialog1.filename = "Search an OVF file"
$openfiledialog1.title = "Select an OVF file"

$WPF_OvfBrowse.Add_Click({
  if ($openfiledialog1.ShowDialog() -eq "OK") {
    #Get the path of specified file
    $WPF_OvfPath.Text = $openfiledialog1.FileName;
  }
})

$openfolderdialog1 = New-Object System.Windows.Forms.FolderBrowserDialog

$WPF_OvaBrowse.Add_Click({
  if ($openfolderdialog1.ShowDialog() -eq "OK") {
    #Get the path of specified folder
    $WPF_OvaPath.Text = $openfolderdialog1.SelectedPath;
  }
})
#endregion 

$WPF_Start.Add_Click({
  Write-Debug "ici"

  $OvaPath=$WPF_OvaPath.Text
  $OvaName=$WPF_OvaName.Text

  #$FinalOvaPath="$OvaPath"+"\"+"$OvaName"+"$ext"
  $FinalOvfPath=$WPF_OvfPath.Text

  Write-Debug $WPF_OvaDir.IsChecked

  if ( ( Validate-IsEmptyTrim($WPF_OvaName.Text) -eq $false ) -and ( $WPF_OvaDir.IsChecked -eq $true ) ) {
    echo 11
    $FinalOvaPath="$OvaPath"+"\"+"$($WPF_OvaName.Text)"+"$ext"
    echo $FinalOvaPath
    #Set-Location $pathTool
    .\tool\ovftool.exe "$FinalOvfPath" "$FinalOvaPath"
  }
  elseif ( ( Validate-IsEmptyTrim($WPF_OvaName.Text) -eq $true ) -and ( $WPF_OvaDir.IsChecked -eq $true ) ) {
    $FinalOvaPath = ChangeExtensionOVFtoOVA($WPF_OvfPath.Text)
    #Set-Location $pathTool
    .\tool\ovftool.exe "$FinalOvfPath" "$FinalOvaPath"
  }

  elseif ( ( Validate-IsEmptyTrim($($WPF_OvaName.Text)) -eq $false ) -and ( $WPF_OvaDir.IsChecked -eq $false ) ) {
    $FinalOvaPath="$OvaPath"+"\"+"$OvaName"+"$ext"
    #Set-Location $pathTool
    .\tool\ovftool.exe "$FinalOvfPath" "$FinalOvaPath"
  }

  else {
    $FinalOvaPath = ChangeExtensionOVFtoOVA($WPF_OvfPath.Text)
    .\tool\ovftool.exe "$FinalOvfPath" "$FinalOvaPath"
  }
  
})


$Form.ShowDialog() | Out-Null

<#
.SelectedItem.ToString()
.Text=
#>